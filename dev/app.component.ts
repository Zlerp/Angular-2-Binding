import {Component} from 'angular2/core';

@Component({
    selector: 'my-app',
    template: `
    <h1>{{stringInt}}</h1>
    <h3>{{1 === 1}}</h3>
    <p>{{name}}</p>
    <p>{{onTest()}}</p>
    <input type="text" value="{{name}}">
    <br>
    --------------------------------------------------------------------
    <br>
    <input type="text" [value]="propBind" [ngClass]="{red: true}" [disabled]=" 1=== 1">
    <br>
    --------------------------------------------------------------------
    <br>
    <input type='text' [placeholder]='typeMe' (keyup)="onKeyup(inputElement.value)" #inputElement>
    <p>{{values}}</p>
    -----------------------------------------------------------
    <h2>Two Way binding!  </h2>
    <br>
    <p> This will not change if you change the top input, but the tops will change on this input. </p>
    <input type="text" placeholder="update old Name" [(ngModel)]="name">
    <p>You're old name: {{name}}  </p>
    <input type="text" placeholder="new Name" [(ngModel)]="newName">
    <p>Your New Name Name: {{newName}}  </p>
    `,
})
export class AppComponent {
  stringInt = "This is String Interpolation"
  name = 'Zach';
  typeMe= 'Type Here';
  propBind = "This is Property Binding.";
  values = 'This is Event Binding: ';
  onTest() {
    return 3 <= 1;
  };
  onKeyup( value: string ){
    this.values += value + ' | ';
  }
}
